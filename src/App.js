import logo from './logo.svg';
import './App.css';
import TryGlasses from './Ex_Try_Glasses/TryGlasses';

function App() {
  return (
    <div className="App">
      <TryGlasses />
    </div>
  );
}

export default App;
