import React, { Component } from 'react'

export default class ItemGlasses extends Component {
    render() {
        let { url, name, price } = this.props.data
        return <div className="col-4 item-glasses">
            <div className="card text-left item-card">
                <img onClick={() => { this.props.handleClick(this.props.data) }} className="card-img-top" src={url} alt />
                <div className="card-body">
                    <h4 className="card-title text-center name">{name}</h4>
                    <p className="card-text text-center price">{price}$</p>
                </div>
            </div>
        </div>
    }
}
