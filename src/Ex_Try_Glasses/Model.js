import React, { Component } from 'react'
// import model from './glassesImage/model.jpg'
export default class Model extends Component {
    render() {
        let { name, desc, url } = this.props.glasses
        return (
            <div className='col-4'>
                <div className="card text-left ">
                    <img className="card-img-top card-model position-relative" src="./glassesImage/model.jpg" alt />
                    <img className='position-absolute card-img' src={url} alt="" />
                    <div className="card-body">
                        <h4 className="card-title">{name}</h4>
                        <p className="card-text">{desc}</p>
                    </div>
                </div>

            </div>
        )
    }
}
