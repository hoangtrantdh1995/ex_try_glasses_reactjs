import React, { Component } from 'react'
import { dataGlasses } from './dataGlasses'
import Header from './Header';
import ItemGlasses from './ItemGlasses';
import Model from './Model';

export default class TryGlasses extends Component {
    state = {
        glassesArr: dataGlasses,
        model: dataGlasses,
    };

    handleChangeModel = (glasses) => {
        this.setState({
            model: glasses,
        })
    }

    renderListGlasses = () => {
        return this.state.glassesArr.map((item, index) => {
            return <ItemGlasses handleClick={this.handleChangeModel}
                data={item} key={index} />
        })
    }

    render() {
        console.table(this.state.glassesArr);
        return (
            <div className='container'>
                <Header />
                <div className='d-flex col-12 p-3'>
                    <Model glasses={this.state.model} />
                    <div className="row col-8">{this.renderListGlasses()}</div>
                </div>
            </div>
        )
    }
}
